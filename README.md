# CSE341-ProgrammingLanguages
Gebze Technical University 2020 Programming Language Course Homeworks 

Homework 1 => Simple Lisp Programming

Homework 2 => FLEX and Lisp (Lexical Analyzer)

Homework 3 => YACC and Lisp (Parser)

Homework 4 => Simple Prolog Programming

Midterm Project => Prolog Interpreter With Lisp Programming Language
